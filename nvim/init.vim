lua require('plugins')

let python_recommended_style=0	" go to hell python. I do what I want

set nonumber				" no line numbers for copy+paste love
set norelativenumber
set showmatch               " show matching 
set noignorecase            " case sensitive 
set noautoindent			" I
set nocindent				" DO NOT
set nosmartindent			" WANT YOU
set softtabstop=0			" DOING WEIRD
set noexpandtab				" AUTO INTENTION
set nosmarttab				" CRAP
set hlsearch                " highlight search 
set incsearch               " incremental search
set shiftwidth=4			" fix stupid 2-tab jumps
set tabstop=4               " number of columns occupied by a tab 
set mouse=					" disable evil mouse
set wildmode=longest,list   " get bash-like tab completions
set cc=100                  " set an 100 column border for good coding style
syntax on                   " syntax highlighting
set clipboard=unnamedplus   " using system clipboard
set cursorline              " highlight current cursorline
set ttyfast                 " Speed up scrolling in Vim
set spell                   " enable spell check (may need to download language package)

colorscheme tokyonight-night
set termguicolors	
