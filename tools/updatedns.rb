#!/bin/ruby

require 'curb'
require 'uri'
require 'net/http'
require 'json'
require 'pp'

@SUBDOMAIN = "subdomain"
@DOMAINID = "1234"
@APIKEY = ""

def get_external_ip(resolve_mode)
  # Get local IP information
  c = Curl::Easy.perform("https://ifconfig.me") do |curl|
    curl.resolve_mode=resolve_mode
  end
  return c.body
end


def get_bunny_records(api_key, domain_id)
  url = URI("https://api.bunny.net/dnszone/#{domain_id}")
  
  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true
  
  request = Net::HTTP::Get.new(url)
  request["accept"] = 'application/json'
  request["AccessKey"] = api_key
  
  response = http.request(request)
  return JSON.parse(response.read_body)["Records"]
end


def update_bunny_record(type, ip)
  bunny_now = get_bunny_records(@APIKEY, @DOMAINID)
  relevant_records = bunny_now.select{ |n| n["Name"] == @SUBDOMAIN and n["Type"] == type }

  if relevant_records.count > 1
   puts "ERROR: Too many type #{type} records!"
   return
  elsif relevant_records.count == 0
    echo "CREATE: Creating dynamic record #{@SUBDOMAIN} / #{type} to #{ip}..."
    url = URI("https://api.bunny.net/dnszone/#{@DOMAINID}/records")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    
    request = Net::HTTP::Put.new(url)
    request["accept"] = 'application/json'
    request["content-type"] = 'application/json'
    request["AccessKey"] = @APIKEY
    request.body = "{\"Type\":#{type},\"Ttl\":300,\"Value\":\"#{ip}\",\"Name\":\"#{@SUBDOMAIN}\",\"Weight\":0,\"Priority\":0,\"Flags\":0,\"Tag\":\"string\",\"Port\":0,\"PullZoneId\":0,\"ScriptId\":0,\"Accelerated\":false,\"MonitorType\":0,\"GeolocationLatitude\":0,\"GeolocationLongitude\":0,\"LatencyZone\":\"string\",\"SmartRoutingType\":0,\"Disabled\":true,\"EnviromentalVariables\":[{\"Name\":\"string\",\"Value\":\"string\"}],\"Comment\":\"string\"}"
    
    response = http.request(request)
    puts response.read_body
  else relevant_records.count == 1
    record = relevant_records.first
    if record["Value"] != ip
      puts "UPDATE: Updating type #{type} to #{ip}..."
      url = URI("https://api.bunny.net/dnszone/#{@DOMAINID}/records/#{record['Id']}")
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(url)
      request["accept"] = 'application/json'
      request["content-type"] = 'application/json'
      request["AccessKey"] = @APIKEY
      request.body = "{\"Value\":\"#{ip}\"}"
      response = http.request(request)
      puts response.read_body
    else
      puts "INFO: Type #{type} already set to #{ip}!"
    end
  end
end

eipv4 = get_external_ip(:ipv4)
eipv6 = get_external_ip(:ipv6)

puts "#{eipv4} / #{eipv6}"

update_bunny_record(0, eipv4)
update_bunny_record(1, eipv6)
