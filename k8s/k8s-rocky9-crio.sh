#!/bin/bash

K8SVERSION="1.26.0"
CRIVERSION="1.24"

echo "Do the k8s shuffle..."

echo "Disable selinux..."

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=disabled/' /etc/selinux/config

echo "Add kubernetes repos..."
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF

echo "Installing kubernetes stuff..."
yum update -y
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sysctl --system

firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=2379-2380/tcp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10252/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
modprobe br_netfilter

cat <<EOF > /var/lib/kubelet/config.yaml
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
EOF
echo "KUBELET_EXTRA_ARGS=\"--container-runtime-endpoint=/var/run/crio/crio.sock --fail-swap-on=0\"" > /etc/sysconfig/kubelet

dnf -y module disable container-tools
dnf -y install 'dnf-command(copr)'
dnf -y copr enable rhcontainerbot/container-selinux
cd /etc/yum.repos.d/
curl -L -o /etc/yum.repos.d/cri-o:$CRIVERSION.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$CRIVERSION/CentOS_8/devel:kubic:libcontainers:stable:cri-o:$CRIVERSION.repo
dnf -y install cri-o

# github.com/cri-o/cri-o/issues/6197
sed -i 's\/"keyPaths\":.*$/\"keyPath\": \"\/etc\/pki\/rpm-gpg\/RPM-GPG-KEY-redhat-release\"/g' /etc/containers/policy.json


sed -i 's|conmon = "/usr/libexec/crio/conmon"|conmon = "/usr/bin/conmon"|g' /etc/crio/crio.conf

rm /etc/cni/net.d/100-crio-bridge.conf

systemctl daemon-reload

systemctl stop firewalld
systemctl disable firewalld
systemctl mask firewalld

systemctl enable crio
systemctl start crio

systemctl enable kubelet

sleep 5

echo "Pulling images..."

# Pull essential system images
kubeadm config images pull

cat <<EOF > /tmp/kubeadm-init-args.conf
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: v$K8SVERSION
apiServer:
  extraArgs:
    authorization-mode: Node,RBAC
    service-node-port-range: 0-32767
networking:
  serviceSubnet: "10.96.0.0/12"
  podSubnet: "10.32.0.0/12"
EOF

# Setup Kubernetes Cluster
kubeadm init --ignore-preflight-errors=NumCPU --config /tmp/kubeadm-init-args.conf

sleep 5

# Setup kubectl
mkdir -p /root/.kube
cp -i /etc/kubernetes/admin.conf /root/.kube/config
chown root:root /root/.kube/config

# No HOME, use KUBECONFIG for now.
export KUBECONFIG=/etc/kubernetes/admin.conf

# Wait for the cluster to be ready

while [ ! $(kubectl get nodes) ]; do echo Waiting on Kubernetes; sleep 1;  done

# WEAVE IS DEAD?!   RIP WEAVE.
# Network Encryption
#echo "" > /var/lib/weave/weave-passwd
#kubectl create secret -n kube-system generic weave-passwd --from-file=/var/lib/weave/weave-passwd
#&password-secret=weave-passwd
# Apply weave networking
#kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPTABLES_BACKEND=nft"

sleep 15

# Untaint Nodes
kubectl taint nodes --all node-role.kubernetes.io/control-plane-

kubectl get nodes -o wide
