#!/bin/bash

echo "Do the k8s shuffle..."

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=disabled/' /etc/selinux/config

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF

yum update -y
yum install -y kubelet kubeadm kubectl tc --disableexcludes=kubernetes

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sysctl --system

firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=2379-2380/tcp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10252/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
modprobe br_netfilter

echo "KUBELET_EXTRA_ARGS=\"--cgroup-driver=systemd\"" > /etc/sysconfig/kubelet

yum -y module disable container-tools
yum -y install 'dnf-command(copr)'
yum -y copr enable rhcontainerbot/container-selinux
cd /etc/yum.repos.d/
curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:1.19.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:1.19/CentOS_7/devel:kubic:libcontainers:stable:cri-o:1.19.repo
yum -y install cri-o

sed -i 's|conmon = "/usr/libexec/crio/conmon"|conmon = "/usr/bin/conmon"|g' /etc/crio/crio.conf

rm /etc/cni/net.d/100-crio-bridge.conf

systemctl daemon-reload
systemctl enable kubelet
systemctl start kubelet
systemctl enable crio
systemctl start crio

sleep 5

cat <<EOF > /tmp/kubeadm-init-args.conf
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: v1.19.3
apiServer:
  extraArgs:
    authorization-mode: Node,RBAC
    service-node-port-range: 0-32767
networking:
  serviceSubnet: "10.96.0.0/12"
  podSubnet: "10.244.0.0/16"
EOF

# Setup Kubernetes Cluster
kubeadm init --ignore-preflight-errors=NumCPU --config /tmp/kubeadm-init-args.conf

sleep 5

# Setup kubectl
mkdir -p /root/.kube
cp -i /etc/kubernetes/admin.conf /root/.kube/config
chown root:root /root/.kube/config

sleep 10

# Setup Flannel
# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
# untaint nodes
# kubectl taint nodes --all node-role.kubernetes.io/master-
# kubectl get nodes -o wide
